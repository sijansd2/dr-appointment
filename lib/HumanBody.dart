import 'package:flutter/material.dart';

class HumanBody extends StatelessWidget {
  VoidCallback imagePressed;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 30.0),
                child: Text(
                  "Whats wrong with you?",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  "Front side",
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
              ),
              SelectedBodyImage(),
              MyButton(),
            ],
          ))),
    );
  }

  void changeSelectedImage() {}
}

class MyButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('MyButton was tapped!');
      },
      child: Container(
        height: 40.0,
        padding: const EdgeInsets.all(8.0),
        margin: const EdgeInsets.only(top: 10.0, right: 50, left: 50),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25.0),
          color: Colors.purple,
        ),
        child: Center(
          child: Text(
            'Get Start Now',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}

class SelectedBodyImage extends StatefulWidget {
  static _SelectedBodyImage of(BuildContext context) =>
      context.ancestorStateOfType(const TypeMatcher<_SelectedBodyImage>());
  @override
  _SelectedBodyImage createState() => _SelectedBodyImage();
}

class _SelectedBodyImage extends State<SelectedBodyImage> {
  String imageName = "assets/images/body.png";
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Stack(
      children: <Widget>[
        Container(
          width: width,
          height: 350,
          //child: Image.asset("assets/images/body.png"),
          child: Image.asset(imageName),
        ),
        Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new GestureDetector(
                    onTap: () {
                      print("Container clicked");
                      changeImage("assets/images/face.png");
                    },
                    child: Opacity(
                      opacity: 0.0,
                      child: Container(
                        height: 60,
                        width: 40,
                        color: Colors.green,
                      ),
                    )),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      print("Container clicked");
                      changeImage("assets/images/right_shoulder.png");
                    },
                    child: Opacity(
                      opacity: 0.0,
                      child: Container(
                        height: 50,
                        width: 50,
                        color: Colors.red,
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      print("Container clicked");
                      changeImage("assets/images/left_shoulder.png");
                    },
                    child: Opacity(
                      opacity: 0.0,
                      child: Container(
                        height: 50,
                        width: 50,
                        color: Colors.yellow,
                      ),
                    )),
              ],
            ),
          ],
        )
      ],
    );
  }

  void changeImage(String path) {
    setState(() {
      imageName = path;
    });
  }
}

LinearGradient purpleGradient(int highColor, int lowColor){
    return new LinearGradient(
      begin: const Alignment(0.0, -1.0),
      end: const Alignment(0.0, 1.0),
      colors: <Color>[
        Colors.purple[highColor],
        Colors.purple[lowColor]
      ],
    );
  }
